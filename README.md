# Pandora

Pandora is an Role Play Game (RPG) based visual novel iOS app that unfolds the story about Pandora, a post-apocalyptic city and it's diplomatic
relations with other possible nations.

# Motivation
Of all the honesty, prior using Swift, my first programming language was C++ and building games with it was totally mesmerizing even though it runs on terminal based.
It was Robert Dawson's Introduction to C++ with Game Programming that led me to dig deeper about how to make games, and after C++, I went to Coffeescript and build
one game prototype which can be seen in this demo link (https://dribbble.com/shots/4238211-Alien-Shooter). Then, I decided to do the same in Swift which ended up
in this project. It's not perfect, I know, but at least I got the grasp of it. It's really fun, honestly !

## Why I Build This App?
This is actually part of my iOS Bootcamp project, but I decided to use my creativity to create better gaming experience that combines better experience, interface 
and as well as storyline, for the most important part indeed. 

### Motivation

### Prerequisites
```
Xcode 10.0
Swift 4.0 / 4.2 
iOS 12
```

## Built With

* [MNIST Digit Classifier](http://yann.lecun.com/exdb/mnist/) - CoreML Model Used
* [CoreML Model Libraries](https://coreml.store/) - CoreML Model Used


## Acknowledgments
* Amazing CoreML Model Libraries !! (https://coreml.store/)

